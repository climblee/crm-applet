// #ifdef MP-BAIDU
const envEnum = {
	development: 'development',
	trial: 'trial',
	production: 'production'
}
const res = swan.getEnvInfoSync();
const env = res.env;
const curEnv = envEnum[env];
// #endif
// #ifdef MP-WEIXIN
const envEnum = {
	develop: 'development',
	trial: 'trial',
	release: 'production'
}
const res = wx.getAccountInfoSync();
const env = res.miniProgram.envVersion;
const curEnv = envEnum[env];
// #endif
// 配置文件
const host = {
	development: {
		// api: 'https://wddev-bookapi.book118.com',
		api: 'https://dev-api.book118.com',
		upload: 'https://api.book118.com',
		// openapi: 'https://openapi-dev.book118.com'
		openapi: 'http://lp.api.book118.com:8080',
		max: 'https://p.book118.com'
	},
	trial: {
		api: 'https://pubapi.book118.com',
		upload: 'https://api.book118.com',
		openapi: 'https://pub-openapi.book118.com',
		max: 'https://p.book118.com'
	},
	production: {
		api: 'https://api.book118.com',
		upload: 'https://api.book118.com',
		openapi: 'https://openapi.book118.com',
		max: 'https://max.book118.com'
	}
}
// const CURENV = 'development';//开发版
const CURENV = 'trial'; //体验版
// const CURENV = 'production';//线上版
const config = {
	// 当前环境
	env: curEnv,
	host: host[CURENV],
	title: '知海行家',
	app: 'knowledge_MP',
	file: {
		uploadSize: 5
	},
	// 客服联系方式
	customer: {
		mobile: '400-0500-739',
		admin: '2850308160',
		qq_operate: '2850308155', //运营QQ
		qq_product: '2850308155', //产品QQ群
		qq_group: '280706131', //QQ群
	},
	// 协议等地址
	article: {
		public: `https://mp.weixin.qq.com/s/l-W-Kqtu9l1GhCUlmzlCdg`, //公众号
		task: `${host[CURENV].max}/college/category30/article748.html`, // 悬赏须知
		orders: `${host[CURENV].max}/college/category30/article748.html`, //接单须知
		serverInfo: `${host[CURENV].max}/college/category30/article714.html`, // 服务信息展示说明
		serverType: `${host[CURENV].max}/college/category30/article715.html`, //服务分类
		serverKeyWord: `${host[CURENV].max}/college/category30/article716.html`, //服务关键词
		booth: `${host[CURENV].max}/college/category30/article710.html`, //推广展位
		skill: `${host[CURENV].max}/college/category30/article711.html` //收益技巧
	},
	// 异常图片地址配置
	errorImage: {
		avator: 'data:image/png;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAgQDAgICAgUEBAMEBgUGBgYFBgYGBwkIBgcJBwYGCAsICQoKCgoKBggLDAsKDAkKCgr/2wBDAQICAgICAgUDAwUKBwYHCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgr/wgARCAAwADADAREAAhEBAxEB/8QAHAABAAICAwEAAAAAAAAAAAAAAAUGCAkBAwcE/8QAHAEBAAICAwEAAAAAAAAAAAAAAAQFBgcBAgMI/9oADAMBAAIQAxAAAADd5klOAABDTI2POyMUjJfjkhrHLrFXTBX7GJ6fidyc+YZXTWCvlAWqnm88KpcwgMK97YZP0cuS8+9KyOBmxobNBq++tNZfBI6Dv8+dpPyPs/8A/8QAMBAAAgEDAQUGBAcAAAAAAAAAAQIDBAURBgAHCBITECExQWFxICNRgRQiJDJikbH/2gAIAQEAAT8A+PXmq00RpCu1Q9MZjSxryRZxzMzBVB9Mkbas3jbytcVKV1dUVUUSr8mGhR44wD5gDx9yTtSX3X1qnFfSXS6wvF39TqyYGPr6bbq9W1OttCUOoq5AtRKjJPyjALoxUsPfGfv270NL1usdBXHTltdRPURoYec4BZHVwM+WeXH32hiigiWGGMIiAKiIMBQO4ADZ40lRo5FBUjBBGQRtuz0pU6L0fBYawp1Ukld1j/anNIzBR7AgfBb62GvpxNE4JwOdfNTtW1kNDTtPM3h4L5k/TbJOT28Q/GXvPuW8KvsG7rUUlntFrqnpoTSKvUqmRiplZyCcEg4AwMeOTtwucX9upp62z8QG8K5wvJKr2+8mBplQYw0TdMcyjIBBAI7znbir419O1dlodGcOurri80daKm56hCPH1AEdRCvUAZhliT+UAcq4JydtwHGdvUs+vqCy7wdRPebRcKuOCp/FovUp+dgolRlGe4kEqcgj17dVtz6pubc3Nm4THm5s5+Y3qc/2ffttHddaY5x+oTz/AJe4/wBHZ//EACsRAAEDAwEFCAMAAAAAAAAAAAECAxEEBSEABhAxQWEHEhMgcYGhsTNykf/aAAgBAgEBPwDz26kNfWIYBiefQZPwNUdstdAgpQATzJgnS6e3vJKFJSZ6DV2o00FetlHAZHoc77TVoorgh5fATPuCPidElRk50MaulYiurVOo4GB/BE+/kW2W1EHTaC4uB5NmNhrS1bEPVrfiOLAJmYTOQAOnMnW2vZ87UlD9maTgQpAIBOZBE46ETrYns9fpnV1N5aTBEJQSDznvGMDhjJ4mdbR7C2h+2uO0bfhuIBIjgYkwQTGeR30Qijb/AFH17fQ3u/hV6Hd//8QAKREAAgECBAUDBQAAAAAAAAAAAQIEAxEABSExEBIgQWEycYEzUaHB8f/aAAgBAwEBPwDrk1xGjtU3t/MV5cySbm/xcDC1ZSG4J098Qa5kxVqHfv8AHGZQaRFamu5/RvhQALDGlsQ6Bjxwh8/k9CMGW+GYIpJ6J+cSmkMtJuVRpp384ynOkTmSU512O/xpjNs7pugpxGO9ydvvpriBnEpK4Wq3MpNte3njV+q3ueKeocP/2Q==',
		img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC4AAAAiAQMAAAAatXkPAAAABlBMVEUAAADMzMzIT8AyAAAAAXRSTlMAQObYZgAAAIZJREFUCNdlzjEKwkAUBNAfEGyCuYBkLyLuxRYW2SKlV1JSeA2tUiZg4YrLjv9PGsHqNTPMSAQuyAJgRDHSyvBPwtZoSJXakeJI9iuRLGDygdl6V0yKDtyMAeMPZySj8yfD+UapvRPj2JOwkyAooSV5IwdDjPdCPspe8LyTl9IKJvDETKKRv6vnlUasgg0fAAAAAElFTkSuQmCC'
	}
}
export default config;
