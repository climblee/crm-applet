#### 使用示例
```
<template>
	<weixin-login ref="wxLogin" :o="options" loginMsg="为了方便您再次阅读和下载，需要登录，请点击" :isShift="true" :loadType="loadType" @loginSuccess="loginSuccess" />
</template>
<script>
	export default {
		data(){
			return {
				options:{
					from:'m', //只要有值代表从其他端进入的
					tmp_token: null, //只要有值，该值就是其他端登录所带过来的token
					login_sync_token: null, //其他端没登录，login_sync_token就可以实现同步登录
				},
				loadType: 1, // 获取用户信息的时候请求，是否显示加载状态 0-不显示  1-显示  默认1
			}
		},
		methods:{
			loginSuccess(){
				console.log('登录成功回调')
			}
		}
	}
</script>
```