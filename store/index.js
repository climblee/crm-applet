import { createStore } from 'vuex';
import getters from '@/store/getters.js';
const modulesFiles = import.meta.globEager('./modules/*.js');
const modules = Object.keys(modulesFiles).reduce((modules, path) => {
	const moduleName = path.replace(/^\.\/modules\/(.*)\.\w+$/, '$1')
	modules[moduleName] = modulesFiles[path]?.default
	return modules
}, {}
)
const store = createStore({
	modules,
	getters
})
export {
	store
}
