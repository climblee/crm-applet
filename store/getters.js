const getters = {
	token: state => state.user.token,
	role: state => state.user.role,
	userInfo: state => state.user.userInfo
}
export default getters;
