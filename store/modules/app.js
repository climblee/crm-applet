// import { request } from '@/common/js/request.unicloud.js'
import { getUserInfo } from '@/common/api/user.js';
// #ifndef VUE3
const statConfig = require('uni-stat-config').default || require('uni-stat-config');
// #endif
export default {
	namespaced: true,
	state: {
		inited: false,
		navMenu: [],
		routes: [],
		appName: process.env.VUE_APP_NAME || process.env.UNI_APP_NAME || '',
		// #ifndef VUE3
		appid: statConfig && statConfig.appid || ''
		// #endif
		// #ifdef VUE3
		appid: process.env.UNI_APP_ID || ''
		// #endif
	},
	mutations: {
		SET_APP_NAME: (state, appName) => {
			state.appName = appName
		},
		SET_NAV_MENU: (state, navMenu) => {
			state.inited = true
			state.navMenu = navMenu
		},
		SET_ROUTES: (state, routes) => {
			state.routes = routes
		}
	},
	actions: {
		init({
			commit
		}) {
			return new Promise((resolve,reject)=>{
				getUserInfo().then(res => {
					const userInfo = res.data;
					commit('user/SET_USER_INFO', userInfo, {
						root: true
					})
					const localRole = uni.getStorageSync('role');
					const role = localRole == 'merchant' && !userInfo.is_merchant ? 'user' : localRole == 'user' ? 'user' : userInfo.is_merchant == 0 ? 'user' : 'merchant';
					commit('user/SET_ROLE', role, {
						root: true
					});
					resolve(true);
					// commit('SET_NAV_MENU',null);
				}).catch(err => {
					// commit('SET_NAV_MENU',null);
					reject(false);
				})
			})
		},
		setAppName({
			commit
		}, appName) {
			commit('SET_APP_NAME', appName)
		},
		setRoutes({
			commit
		}, routes) {
			commit('SET_ROUTES', routes)
		}
	}
}
