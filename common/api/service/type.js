import Request from '@/common/utils/request.js';
/**
 * 获取服务分类
 */
export function getServiceType() {
	return Request({
		url: '/crm/search/getAllSort',
		method: 'GET'
	})
}
