// 用户数据
import Request from '@/common/utils/request.js';
/**
 * 获取用户信息
 */
export function getUserInfo() {
	return Request({
		url: '/crm/user/getUserInfo',
		method: 'GET'
	})
}
/**
 * 退出登录
 */
export function logOut() {
	return Request({
		url: '/crm/user/logout',
		method: 'GET',
		type: 3,
		bMsg: '退出中',
		eMsg: '退出成功'
	})
}
/**
 * 获取用户微信unionid等相关信息
 * @param { Object } data
 * @param { String } data.code login获取的code
 * @param { String } data.iv getUserInfo获取的加密参数iv
 * @param { String } data.ciphertext getUserInfo获取的加密参数encryptedData
 * @return { Object } unionid相关信息
 */
export function getUserWxInfo(data, type = 1) {
	return Request({
		url: "/crm/login/getWeixinInfoByCode",
		data: data,
		bMsg: '加载中',
		type: type
	})
}
/**
 * 获取用户手机号
 * @param { Object } data
 * @param { String } data.code login获取的code
 * @param { String } data.iv 手机号授权获取的加密参数iv
 * @param { String } data.ciphertext 手机号授权获取的加密参数encryptedData
 * @return { Object } unionid相关信息
 */
export function getUserWxMobile(data) {
	return Request({
		url: "/crm/login/getMobileInfoByCode",
		data: data,
		type: 1,
		bMsg: '加载中'
	})
}
/**
 * 判断用户是否绑定公众号
 * @param { Object } data
 * @param { String } data.token 登录用户token
 */
export function checkBindAccount() {
	return Request({
		url: "/applet/Weixinappletlogin/isShowSubscribe",
		type: 0
	})
}
/**
 * 检测账号绑定
 * @param { Object } data
 * @param { String } data.tmp_token 其他端来的token
 * @return { Object }
 */
export function checkBind(data) {
	return Request({
		url: "/crm/login/checkBind",
		data: data,
		type: 0
	})
}
/**
 * 获取商户审核状态
 * @param { Object } data
 * @param { String } token 登录商户的token
 * @return { Object }
 */
export function getMerchantType() {
	return Request({
		url: '/crm/Business/index',
		type: 1,
		bMsg: '加载中'
	})
}
/**
 * 微信、手机号绑定
 * @param { Object } data
 * @param { String } data.mobile 绑定的手机号
 * @param { String } data.weixin_info 微信信息
 * @param { String } data.tmp_token 其他端来的token
 * @return { Object }
 */
export function bind(data) {
	return Request({
		url: "/crm/login/bind",
		data: data,
		type: 0
	})
}
/**
 * 最终执行登录
 * @param { Object } data
 * @param { String } data.mobile 手机号
 * @param { String } data.weixin_info 微信一系列信息
 * @param { String } data.mid 选择账号的mid
 * @return { Object }
 */
export function wxLogin(data, type) {
	return Request({
		url: `/crm/login/${type}`,
		data: data,
		type: 1,
		bMsg: '登录中',
		eMsg: '登录成功'
	})
}
/**
 * 微信、手机号绑定
 * @param { Object } data
 * @param { String } data.login_sync_token 小程序登录成功后的token
 * @return { Object }
 */
export function loginSync(data) {
	return Request({
		url: "/crm/login/loginSync",
		data: data,
		type: 0
	})
}
/**
 * 获取用户是否有最新消息
 */
export function getUserImNews() {
	return Request({
		url: "/chat/MsgInfo/getUserLastTimeUnread",
		type: 0
	})
}
