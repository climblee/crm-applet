// 统计
import Request from '@/common/utils/request.js';
/**
 * 统计
 * @param { String } data.type im-商家咨询量；service-服务；mobile-手机号；weixin-微信；space-空间访问量
 * @param { String } data.merchant_mid 商家的mid
 * @param { String } data.service_id 服务id
 * @return { Boolean }
 */
export function tongji(data) {
	return Request({
		url: '/crm/search/hitTongji',
		method: 'GET',
		data
	})
}
