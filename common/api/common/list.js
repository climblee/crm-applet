import Request from '@/common/utils/request.js';
/**
 * 通用获取列表数据
 * @param {Object} o
 */
export function getList(o) {
	let {
		url,
		method = 'POST',
		data,
		cType = 1,
		host
	} = o;
	return Request({
		url: url,
		method: method,
		data: data,
		cType,
		host
	})
}
