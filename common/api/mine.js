// 用户中心数据
import Request from '@/common/utils/request.js';
/**
 * 获取用户中心推荐商家
 */
export function getRecommoned() {
	return Request({
		url: '/crm/user/recommendMechant',
		method: 'GET'
	})
}
/**
 * 获取用户中心最新悬赏
 */
export function getNewTask() {
	return Request({
		url: '/crm/user/newTask',
		method: 'GET'
	})
}