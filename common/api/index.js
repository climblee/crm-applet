// 首页数据
import Request from '@/common/utils/request.js';
/**
 * 获取服务分类
 */
export function getIndexData() {
	return Request({
		url: '/crm/home/index',
		method: 'GET'
	})
}
