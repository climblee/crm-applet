/**
 * 提示框
 * @param { Object } o
 * @param { String } o.title 标题
 * @param { String } o.content 内容
 * @param { String } o.confirmColor 确认按钮颜色
 * @param { String } o.confirmText 确认按钮文字
 * @param { String } o.cancelText 取消按钮文字
 * @param { String } o.cancelColor 取消按钮颜色
 * @param { Boolean } o.showCancel 是否显示取消按钮
 * @param { Function } o.confirm 确认
 * @param { Function } o.cancel 取消
 */
export function showModal(o) {
	const { title = '提示', content = '', confirmColor = '#E1835B', confirmText = '确定', cancelText = "取消", cancelColor = '#aaa', showCancel = true, confirm = Function, cancel = Function } = o;
	uni.showModal({
		title: title,
		content: content,
		confirmColor: confirmColor,
		confirmText: confirmText,
		cancelText:	cancelText,
		cancelColor: cancelColor,
		showCancel: showCancel,
		success(res) {
			if (res.confirm) confirm();
			else cancel();
		}
	})
}
/**
 * 联系QQ，支持复制
 * @param {Object} o.value QQ号码
 * @param {Object} o.title 提示title
 * @param {Object} o.msg 提示文字+QQ号
 */
export function showQQ(o) {
	const { value, title = '提示', msg = '客服QQ号' } = o;
	showModal({
		title,
		content: `${msg}：${value}`,
		confirmText: '复制',
		confirm() {
			showCopy({
				data: value,
				type: 1,
				icon: 'none',
				title: `${msg}已复制`
			})
		}
	})
}
/**
 * 复制
 * @param {Object} o.data 复制的内容
 * @param {Object} o.type 0不显示提示 1显示提示
 * @param {Object} o.title 提示的文案
 * @param {Object} o.icon 提示的类型
 */
export function showCopy(o) {
	const { data, type = 1, title = '复制成功', icon = 'success' } = o;
	return new Promise((resolve, reject) => {
		uni.setClipboardData({
			data: `${data}`,
			success: res => {
				setTimeout(() => {
					if (type == 1) uni.showToast({
						icon,
						title
					})
					resolve(data);
				}, 10)
			},
			fail: error => {
				uni.showToast({
					icon: 'none',
					title: '复制失败'
				})
				reject('复制失败');
			},
			complete: res => {
				uni.hideToast();
			}
		})
	})
}
