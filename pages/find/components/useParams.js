import { reactive, toRefs } from 'vue';
import { deepClone, getStorage, setStorage } from '@/common/utils/index.js';
import { getServiceType } from '@/common/api/service/type.js';
export default function() {
	const defaultData = {
		service: {
			list: [{ name: '不限', value: 'all', list: [{ name: '全部分类', value: 'all' }] }],
			value: { name: '服务分类', value: 'all' }
		},
		auth: {
			list: [{ name: '全部', value: 'all' }, { name: '机构认证', value: '2' }, { name: '个人认证', value: '1' }],
			value: { name: '认证类型', value: 'all' }
		},
		price: {
			list: [{ name: '不限', value: 'all' }, { name: '50以下', value: '0,50' }, { name: '50-100', value: '50,100' }, { name: '100-200', value: '100,200' }, { name: '200-500', value: '200,500' }, {
				name: '500-1000',
				value: '500,1000'
			}, { name: '1000以上', value: '1000,99999' }],
			value: { name: '价格区间', value: 'all' }
		},
		order: {
			list: [{ name: '默认排序', value: 'default' }, { name: '访问量优先', value: 'kpv' }, { name: '咨询量优先', value: 'cv' }],
			value: { name: '默认排序', value: 'default' }
		},
		serve_order: {
			list: [{ name: '默认排序', value: 'default' }, { name: '价格低优先', value: 'serviceprice' }, { name: '最热优先', value: 'cv' }, { name: '最新优先', value: 'id' }],
			value: { name: '默认排序', value: 'default' }
		},
		more: {
			list: [{ name: '服务中', value: 'is_work' }],
			value: { name: '更多筛选', value: 'default' }
		}
	}
	const data = reactive({
		service: {
			list: [],
			value: {}
		},
		auth: deepClone(defaultData.auth),
		price: deepClone(defaultData.price),
		order: deepClone(defaultData.order),
		serve_order: deepClone(defaultData.serve_order),
		more: deepClone(defaultData.more),
		params: {}
	});
	// 获取服务分类数据
	const getService = async () => {
		const key = 'SERVICE_TYPES';
		data.service = deepClone(defaultData.service);
		const localServiceTypes = getStorage(1, key);
		if (!localServiceTypes) {
			const reqTypes = await getServiceType();
			for (let key in reqTypes.data) {
				data.service.list.push({ name: reqTypes.data[key].name, value: reqTypes.data[key].id, list: reqTypes.data[key].children.map(item => { return { name: item.sortname, value: item.id } }) });
			}
			setStorage(data.service.list, key);
		} else {
			data.service.list = localServiceTypes;
		}
	}
	const callbackSelect = (item) => {
		if (item.type == 'service') {
			data.params.type = item.value;
		} else if (item.type == 'serve_order') {
			data.params.order = item.value;
		} else {
			data.params[item.type] = item.value;
		}
		data[item.type].value = ['all', 'default'].indexOf(item.value) > -1 ? deepClone(defaultData[item.type].value) : {
			name: item.name,
			value: item.value
		};
		// if (['default', 'all'].indexOf(item.value) > -1) delete data.params[item.type == 'service' ? 'type' : item.type];
	}
	const callbackMore = (arr) => {
		arr.forEach((item, index) => {
			data.params[item.value] = item.selected ? 1 : 0;
			// if (['default', 'all', 0].indexOf(data.params[item.value]) > -1) delete data.params[item.value];
		})
		const selected = arr.filter(item => item.selected);
		data.more.value = {
			name: selected.length > 0 ? selected.length > 1 ? `${selected[0].name}...` : selected[0].name : '更多筛选',
			value: selected.length > 0 ? 1 : 'all'
		}
	}
	const searchChange = (o) => {
		data.params.q = o.value ? o.value : '';
	}
	return {
		getService,
		callbackSelect,
		callbackMore,
		searchChange,
		...toRefs(data)
	}
}
