import { reactive, toRefs } from 'vue';
import { deepClone, getStorage, setStorage } from '@/common/utils/index.js';
export default function() {
	const defaultData = {
		task_status: {
			list: [{ name: '全部', value: 'all' }, { name: '进行中', value: '1' }, { name: '已完成', value: '2' }],
			value: { name: '状态', value: 'all' }
		},
		order: {
			list: [{ name: '排序', value: 'default' }, { name: '抢单人少', value: 'takeorders' }, { name: '截止时间晚', value: 'deadline' }],
			value: { name: '排序', value: 'default' }
		},
		price: {
			list: [{ name: '不限', value: 'all' }, { name: '50以下', value: '0-50' }, { name: '50-100', value: '50-100' }, { name: '100-200', value: '100-200' }, { name: '200-500', value: '200-500' }, {
				name: '500-1000',
				value: '500-1000'
			}, { name: '1000以上', value: '1000-99999' }],
			value: { name: '价格区间', value: 'all' }
		}
	}
	const data = reactive({
		task_status: deepClone(defaultData.task_status),
		order: deepClone(defaultData.order),
		price: deepClone(defaultData.price),
		params: {}
	});
	const callbackSelect = (item) => {
		data.params[item.type] = item.value;
		data[item.type].value = ['all', 'default'].indexOf(item.value) > -1 ? deepClone(defaultData[item.type].value) : {
			name: item.name,
			value: item.value
		};
	}
	const searchChange = (o) => {
		data.params.q = o.value ? o.value : '';
	}
	return {
		callbackSelect,
		searchChange,
		...toRefs(data)
	}
}
