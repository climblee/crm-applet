import { reactive, toRefs } from 'vue';
import { deepClone, getStorage, setStorage } from '@/common/utils/index.js';
export default function() {
	const defaultData = {
		contact_status: {
			list: [{ name: '全部', value: 'all' }],
			value: { name: '全部状态', value: 'all' }
		},
		user_from: {
			list: [{ name: '全部', value: 'all' }],
			value: { name: '全部来源', value: 'all' }
		}
	}
	const data = reactive({
		contact_status: deepClone(defaultData.contact_status),
		user_from: deepClone(defaultData.user_from),
		params: {}
	});
	const callbackSelect = (item) => {
		data.params[item.type] = item.value;
		data[item.type].value = ['all', 'default'].indexOf(item.value) > -1 ? deepClone(defaultData[item.type].value) : {
			name: item.name,
			value: item.value
		};
	}
	return {
		callbackSelect,
		...toRefs(data)
	}
}
