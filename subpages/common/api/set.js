import Request from '@/common/utils/request.js';
/**
 * 获取商户设置页面审核状态
 */
export function getMerchantExamineType() {
	return Request({
		url: '/crm/Business/index',
		method: 'GET',
	})
}
/**
 * 获取商户基本信息
 */
export function getBusinessInfo() {
	return Request({
		url: '/crm/Business/getBusinessInfo',
		method: 'POST',
	})
}
/**
 * 修改商户基本信息
 */
export function editBusinessInfo(data) {
	return Request({
		url: '/crm/Business/editBusinessMes',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
		eMsg: '提交成功！请等待管理员审核',
		iconType: 'none',
	})
}
/**
 * 获取客户手机号
 */
export function getPhoneNumber() {
	return Request({
		url: '/crm/Business/getMobile',
		method: 'POST',
	})
}
/**
 * 更改客户手机号是否对外显示
 */
export function editPhoneNumber(data) {
	return Request({
		url: '/crm/Business/editBusinessMobile',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
		eMsg: '修改成功',
	})
}
/**
 * 获取客户微信号
 */
export function getWechat() {
	return Request({
		url: '/crm/Business/getWechat',
		method: 'POST',
	})
}
/**
 * 修改客户微信号
 */
export function editWechat(data) {
	return Request({
		url: '/crm/Business/editBusinessWechat',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
		iconType: 'none',
		eMsg: '提交成功！请等待管理员审核',
	})
}
/**
 * 获取客户证书
 */
export function getCredentials() {
	return Request({
		url: '/crm/Business/getCredentials',
		method: 'POST',
	})
}
/**
 * 修改客户证书
 */
export function editCredentials(data) {
	return Request({
		url: '/crm/Business/editBusinessCredentials',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
		iconType: 'none',
		eMsg: '提交成功！请等待管理员审核',
	})
}
/**
 * 获取服务信息
 */
export function getBusinessSvc() {
	return Request({
		url: '/crm/Business/businessSvc',
		method: 'POST',
	})
}
/**
 * 修改服务时间
 */
export function setSvcTime(data) {
	return Request({
		url: '/crm/Business/setSvcTime',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
		iconType: 'none',
		eMsg: '修改成功',
	})
}
/**
 * 获取服务行业
 */
export function getMaxClassList() {
	return Request({
		url: '/crm/Maxclass/maxClassList',
		method: 'GET',
	})
}
/**
 * 修改服务行业
 */
export function editTrade(data) {
	return Request({
		url: '/crm/Business/editTrade',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
		iconType: 'none',
		eMsg: '修改成功',
	})
}
/**
 * 获取服务分类
 */
export function getSortList() {
	return Request({
		url: '/crm/Sort/sortList',
		method: 'GET',
	})
}
/**
 * 修改服务分类
 * */
export function editSort(data) {
	return Request({
		url: '/crm/Business/editClassify',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
		iconType: 'none',
		eMsg: '修改成功',
	})
}
/**
 * 修改服务关键词
 */
export function editLabels(data) {
	return Request({
		url: '/crm/Business/editLabels',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
		iconType: 'none',
		eMsg: '修改成功',
	})
}
/**
 * 辅助功能展示页
 */
export function greetings() {
	return Request({
		url: '/crm/Business/greetings',
		method: 'GET',
	})
}
/**
 * 招呼语列表
 */
export function greetingsLan() {
	return Request({
		url: '/crm/Business/greetingsLan',
		method: 'POST',
	})
}
/**
 * 打招呼语设置
 */
export function greetingsSet(data) {
	return Request({
		url: '/crm/Business/greetingsSet',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
		iconType: 'none',
		eMsg: '修改成功',
	})
}
/**
 * 开启自动打招呼
 */
export function startAutogreetings(data) {
	return Request({
		url: '/crm/Business/startAutogreetings',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
		iconType: 'none',
		eMsg: '开启成功',
	})
}
/**
 * 关闭自动打招呼
 */
export function closeAutogreetings() {
	return Request({
		url: '/crm/Business/closeAutogreetings',
		method: 'POST',
		type: 3,
		bMsg: '提交中',
		iconType: 'none',
		eMsg: '关闭成功',
	})
}
/**
 * 是否关注微信公众号
 */
export function isBindSubscribe() {
	return Request({
		url: '/crm/Business/isBindSubscribe',
		method: 'GET',
		failBack: 1
	})
}
