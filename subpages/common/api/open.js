import Request from '@/common/utils/request.js';
/**
 * 立即开通（开通前鉴权）
 */
export function activateNow() {
	return Request({
		url: '/crm/Business/activateNow',
		method: 'GET',
		failBack: 1
	})
}
/**
 * 立即开通
 */
export function applyBusiness(data) {
	return Request({
		url: '/crm/Business/applyBusiness',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
	})
}
/**
 * 下载承诺书模板
 */
export function downloadFile() {
	return Request({
		url: '/crm/Business/downloadFile',
		method: 'GET',
		type: 3,
		bMsg: '下载中',
		iconType: 'none',
		eMsg: '下载成功',
	})
}
