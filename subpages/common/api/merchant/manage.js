import Request from '@/common/utils/request.js';
/**
 * 上下架
 * @param { String } data.serviceid 服务id
 * @param { String } data.token token
 * @return { Object }
 */
export function changeServiceState(data, eMsg) {
	return Request({
		url: '/crm/Service/isshow',
		data,
		type: 3,
		bMsg: '请求中',
		eMsg: eMsg,
		mask: true
	})
}
/**
 * 服务预览
 * @param { String } data.serviceid 服务id
 * @return { Object }
 */
export function getServiceDetail(data) {
	return Request({
		url: '/crm/Service/preview',
		data,
		type: 1,
		bMsg: '请求中',
		mask: true
	})
}
