import Request from '@/common/utils/request.js';
/**
 * 打招呼-客户大厅
 * @param { Object } data
 * @param { String } data.token 登录用户token
 * @param { String } data.greetings 招呼语
 * @param { String } data.defaultgreetings 是否自动发送
 * @param { String } data.uid 用户ID
 * @return { Object }
 */
export function call_hall(data) {
	return Request({
		url: "/crm/Custom/hallCall",
		method: 'POST',
		data,
		bMsg: '发送中',
		notFailMsg: true,
		failBack: true,
		type: 1
	})
}
/**
 * 打招呼-我的客户
 * @param { Object } data
 * @param { String } data.token 登录用户token
 * @param { String } data.greetings 招呼语
 * @param { String } data.defaultgreetings 是否自动发送
 * @param { String } data.uid 用户ID
 * @return { Object }
 */
export function call_my(data) {
	return Request({
		url: "/crm/Custom/call",
		method: 'POST',
		data,
		bMsg: '发送中',
		notFailMsg: true,
		failBack: true,
		type: 1
	})
}
