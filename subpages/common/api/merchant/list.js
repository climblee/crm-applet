import Request from '@/common/utils/request.js';
/**
 * 打招呼-客户大厅
 * @param { String } data.uid uids
 * @return { Array }
 */
export function checkDataState(data) {
	return Request({
		url: "/crm/Custom/clickHallCall",
		method: 'POST',
		data,
		type: 0
	})
}
