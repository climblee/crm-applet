// 请求富文本相关的接口
import Request from '@/common/utils/request.js';
/**
 * 获取商户服务协议
 */
export function merchantAgreement() {
	return Request({
		url: '/crm/Business/agreement',
		method: 'GET',
		type: 1,
		bMsg: '加载中'
	})
}
