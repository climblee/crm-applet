import Request from '@/common/utils/request.js';
/**
 * 商户-需求详情
 * @param { String } data.taskid 任务id
 * @param { String } data.is_winner 是否是选中的商家。all-所有接单商家；1-选中的商家；0-未选的商家
 */
export function getDetail(data, type = 1) {
	return Request({
		url: '/crm/task/taskdetail',
		method: 'GET',
		data,
		type
	})
}
/**
 * 用户(我)-需求详情
 * @param { String } data.taskid 任务id
 * @param { String } data.page 页码
 */
export function getPublisherDetail(data, type = 1) {
	return Request({
		url: '/crm/task/myPublishTaskdetail',
		method: 'GET',
		data,
		type
	})
}
/**
 * 用户(我)-关闭需求
 * @param { String } data.taskid 任务id
 * @param { String } data.page 页码
 */
export function userFinishTask(data) {
	return Request({
		url: '/crm/task/userFinishTask',
		method: 'GET',
		data,
		type: 3,
		bMsg: '关闭中',
		eMsg: '需求关闭'
	})
}
/**
 * 用户(我)-选中商家
 * @param { String } data.taskid 任务id
 * @param { String } data.merchant_mid 商家mid
 */
export function userSelectMerchant(data) {
	return Request({
		url: '/crm/task/userSelectMerchant',
		method: 'GET',
		data,
		type: 3,
		bMsg: '提交中',
		eMsg: '选人成功',
		mask: true
	})
}