import Request from '@/common/utils/request.js';
/**
 * 商家-获取需求方手机号
 * @param { String } data.taskid 任务id
 */
export function getPublisherMobile(data) {
	return Request({
		url: '/crm/task/getPublisherMobile',
		method: 'GET',
		data,
		type: 1,
		bMsg: '获取中',
		mask: true
	})
}