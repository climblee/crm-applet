import Request from '@/common/utils/request.js';
/**
 * 确认抢单
 * @param { String } data.task_time 时间
 * @param { String } data.task_price 价格
 * @param { String } data.task_intr 说明
 */
export function merchanttakeorders(data) {
	return Request({
		url: '/crm/task/merchanttakeorders',
		method: 'POST',
		data,
		type: 3,
		iconType: 'none',
		bMsg: '提交中',
		eMsg: '提交成功！请等待审核~'
	})
}
