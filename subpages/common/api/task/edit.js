import Request from '@/common/utils/request.js';
/**
 * 发布悬赏
 * @param { String } data.title 任务标题
 * @param { String } data.description 任务描述
 * @param { String } data.deadline 任务截止时间，时间戳
 * @param { String } data.price 任务的悬赏价格。|500-1000
 * @param { String } data.task_from 任务来源
 */
export function addTask(data) {
	return Request({
		url: '/crm/task/addtask',
		method: 'POST',
		data,
		type: 3,
		bMsg: '提交中',
		eMsg: '提交成功'
	})
}
/**
 * 需求更新
 * @param { String } data.taskid 任务id
 * @param { String } data.title 任务标题
 * @param { String } data.description 任务描述
 * @param { String } data.deadline 任务截止时间，时间戳
 * @param { String } data.price 任务的悬赏价格。|500-1000
 */
export function updateTask(data) {
	return Request({
		url: '/crm/task/updatetask',
		method: 'POST',
		data,
		type: 3,
		bMsg: '修改中',
		eMsg: '修改成功'
	})
}
