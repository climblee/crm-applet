import Request from '@/common/utils/request.js';
/**
 * 更新头像
 * @param {Object} data
 * @param {String} data.avatar 上传的头像地址-必填
 */
export function updateAvator(data) {
	return Request({
		url: "/app/set/avatar",
		data: data,
		bMsg: '更新中',
		eMsg: '更新成功',
		type: 3
	})
}
/**
 * 同步im的资料
 * @param {Object} data
 */
export function updateImInfos(data) {
	return Request({
		url: "/chat/Profile/portraitSet",
		data: data,
		bMsg: '同步中',
		eMsg: '同步成功',
		type: 3
	})
}
/**
 * 更改昵称
 * @param {Object} data
 * @param {String} data.nickname 昵称
 */
export function updateNickName(data) {
	return Request({
		url: "/app/set/nickname",
		data: data,
		bMsg: '修改中',
		eMsg: '修改成功',
		back: true,
		type: 3
	})
}
/**
 * 更改简介
 * @param {Object} data
 * @param {String} data.brief 简介
 */
export function updateIntroduce(data) {
	return Request({
		url: "/app/set/brief",
		data: data,
		bMsg: '修改中',
		eMsg: '修改成功',
		back: true,
		type: 3
	})
}
