/**
 * 适用小程序/uniapp项目验证
 * @param {Array} array 验证数据
 * @return {Boolean}
 * 使用示例：
 * valid([{
		value: data,
		dataType: 'email',
		null: '请输入邮箱',
		error: '请输入正确的邮箱'
	}],).then(res => {
		console.log(res)
	})
	自定义规则：
	valid([{
			value:'',
			dataType:'kong',
			error:'哈哈哈'
		}],{dataTypes:{'kong':/\S/}}).then(res=>{
		console.log(res)
	})
 * */
export function valid(array, {
	dataTypes = {},
	tip = 1
} = {}) {
	const _dataTypes = {
		'null': /\S/,
		'password': /^[\w\W]{6,16}$/,
		'mobile': /^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$/,
		'code': /^\d{4}$/,
		'email': /\w+((-w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+/,
		'resetWord': /((?=.*[a-z])(?=.*\d)|(?=[a-z])(?=.*[#@!~%^&*])|(?=.*\d)(?=.*[#@!~%^&*]))[a-z\d#@!~%^&*]{8,16}/i,
		'idCard': /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/,
		'company': /^[\w\W]{6,30}$/,
		'num': /^[1-9]\d*$/, //非零的正整数
		...dataTypes
	};
	return new Promise((resolve, reject) => {
		for (let i = 0; i < array.length; i++) {
			let item = array[i];
			const itemValue = isNaN(item.value) ? item.value : /^(0|[1-9][0-9]*)(\.\d+)?$/.test(item.value) ? +item.value : item.value;
			if ((item.null && !_dataTypes.null.test(item.value)) || !item.value) { //统一处理为空的情况
				if (tip == 1) uni.showToast({
					icon: 'none',
					title: item.null ? item.null : '数据为空'
				});
				else uni.showModal({
					showCancel: false,
					content: item.null ? item.null : '数据为空'
				})
				return false;
			} else if (item.dataType && !_dataTypes[item.dataType].test(itemValue)) { //处理error的情况
				if (tip == 1) uni.showToast({
					icon: 'none',
					title: item.error ? item.error : '输入错误'
				})
				else uni.showModal({
					showCancel: false,
					content: item.error
				})
				return false;
			}
		}
		resolve();
	})
}
